/** Express Server | CondinRanking. */

/** Modules. */
let express = require('express');
let bodyParser = require('body-parser');
let app = express();
const axios = require('axios')

/** Express server configuration */
const port = 8000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With,    Content-Type, Accept");
  next();
});

const userId = "efbd5dcfa7827000a92ee17ae0fb2a0c1872043"
const url = "https://www.codingame.com/services/Leaderboards/getFilteredChallengeTeamLeaderboard";
const jsonBody = `["challengeName",${userId},{"active":true,"column":"KEYWORD","filter":"companyName"},"COMPANY",null]`;

/** Getting rank. */
app.get('/:challengeName/:company/:region', async function (req, res) {

  let body = jsonBody.replace("challengeName", req.params.challengeName);
  body = body.replace("companyName", req.params.company);

  if (req.params.region.toUpperCase() != "WORLD")
    body = body.replace("null", req.params.region);

  res.send(await getRanking(url, body, res));
});


/** Make post request to Condingame to get the current rank. */
async function getRanking(url, body, res) {
  let data = "";

  await axios
    .post(url,
      body,
      {
        headers: { 'Content-Type': 'text/plain' }
      })

    .then(result => {
      data = result.data;
    })
    .catch(error => {
      console.error(error)
      res.status(500).send("Server error: Check the name of the challenge or check logs")
    })

  data = JSON.stringify(data).split(']')[0];
  data = data.substring(data.indexOf('[') + 1);
  data += "]}}";

  try {
    return JSON.parse(data);
  }
  catch (Exception) {
    res.status(404).send("Unknow company.")
  }

}

app.listen(port);
console.log(`Server started on port: ${port}`);