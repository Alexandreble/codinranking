
# CodinRanking
API de récupération des classements des entreprises sur les challenges CodingGame.


## Fonctionnement 

CondinRanking simule les appels fait sur le site CodinGame pour le classement des entreprises.

Url d'appel utilisée : (POST) :    `https://www.codingame.com/services/Leaderboards/getFilteredChallengeTeamLeaderboard`

Contenu du body :

    ["challengeName","userId",{"active":true,"column":"KEYWORD","filter":"NoConsulting"},"COMPANY","france"]

> ChallengeName : Nom du challenge définie par le site. Exemple : spring-challenge-2021
> userId : Identifiant d'un compte utilisateur CodingGame. Exemple : 91c2791a62d4a5d50707b89cfb8418102618204
> Null : Peut-être remplacé par l'indentifiant du pays : "FR"

## Lancement

    $ npm install
	$ nodemon Server.js ou $ npm start
	
    

## Consomation

Récupération du classement France :

    GET
    http://localhost:9000/challengeName/Company/GeoZone

    GET
    http://localhost:9000/spring-challenge-2021/NoConsulting/FR



Le résultat d'appel retournera le classement de l'entreprise (France dans cet exemple) ainsi que le classement des joueurs de cette dernière.
On y retrouve aussi la league et le score.

> Exemple de résultat :

    {
    "rank": 91,
    "score": 2128,
	    "team": {
		    "id": 36164,
		    "countryId": "FR",
		    "name": "NoConsulting",
		    "logoBinaryId": 53732949348165,
		    "website": "noconsulting.fr",
			"users": [
				{
				"rank": 2244,
				"league": {
				"divisionIndex": 3,
				"divisionCount": 6,
				"divisionAgentsCount": 1926,
				"openingLeaguesCount": 0,
				"divisionOffset": 0
				},
				"codinGamer": 
					{
					"userId": 3967892,
					"pseudo": "Dodo_Noc",
					"avatar": 49854480664709,
					"publicHandle": "dda423bc119cc519e82da0e6b647628f2987693"
					}
				}, 
				...]
			}
		}

**NoConsutling** est à la place 91 en France avec un score de 2128.
**Dodo_Noc** est le premier résultat du tableau users, il est donc premier de la société NoConsulting. Il est 2244 ème de la league d'argent.

## Erreurs

    404 - Unknow company
    
    
  Si l'entreprise n'existe pas ou est mal orthographiée.
  Cette erreur est retournée aussi dans la cas ou la région est mauvaise.

    500 - Server error: Check the name of the challenge or check logs
L'erreur apparait si le challenge est inconnu.
Elle peut apparaître si CodinGame ne réponds pas.

## Mapping des classements par régions


|Région|Valeur  |
|--|--|
| Monde | WORLD
 |France | FR
 |Allemagne|DE
 |Belgique | BE
 |Norvège|NO
 |Bangladesh|BD	
 |Russie|RU
 |Finlande|FI
 |Tanzanie|TW
 |Japon|JP
 |Lituanie|LT
 |Ukraine|UA
 |Nouvelle-Zélande|NZ
 |Hongrie|HU
 |Brésil|BR
 |Serbie|SE
 |Maroc|MA
 |Bélarus|BY
 |Slovaquie|SK
 |Royaume-Uni|GB
 |Canda|CA
 |Etats Unis|US
 |Egypte|EG
 |Chine|CH
 |Inde|IN
 |Corée,|KR
 |Micronésie|MX
 |Italie|IT
 |Grèce|GR
 |Cocos|CO
 |Espagnee|ES
 |Autriche|AT
 |Australie|AU
 |Thaïlande|TH
 |Tchèque, République|CZ
 |Viet Nam|VN
 |Pologne|PL
 |Pays-Bas|NL
        
 
 
 
 